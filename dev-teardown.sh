#!/bin/sh

# Echo commands
set -x

# Delete helm chart
helm del --purge yoloswag

# Stop minikube vm
minikube stop