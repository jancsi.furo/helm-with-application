#!/bin/sh

# Immediately stop script at any error
set -e

# Print input lines
set -v 

# Start minikube
minikube start

# Set docker env
eval $(minikube docker-env)

# Build the docker image
docker build -t yoloswag:v1 .

# Init helm
helm init


echo "Waiting for kube-proxy to be ready ..."
sleep 30

# wait for kube-proxy to be ready
for i in {1..30}
do
    READY=$(kubectl get daemonset.apps kube-proxy -n kube-system -o 'jsonpath={.status.numberReady}')
    echo "Waiting for kube-proxy to be ready ..."    
    sleep 10
    
    if [ "$READY" -eq 1 ]
    then
        break  
    fi
done

# Install the chart
helm install ./helm-chart/yoloswag --name yoloswag
