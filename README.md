# Helm With Application

This repo contains an application which does the following:

- prints the numbers from 1 to 50, but for multiples of three print “Yolo” instead of the number and for the multiples of five print “Swag”. For numbers which are multiples of both three and five print “YoloSwag”

The app is implemented using Python and Flask, it has a Docker image and a Helm chart that can be deployed on Minikube. After installing the chart on minikube, the list of the requested numbers is accessible through a NodePort service which is shown after the successful helm installation.

The `dev-setup.sh` script starts minikube, builds the image and deploys the helm chart.

The `dev-teardown.sh` script deletes the installed chart and stops miniube.

Documentation can be found under the `docs` folder and on wiki pages:

1. [Hiring task](https://gitlab.com/jancsi.furo/helm-with-application/wikis/Hiring-Task)
2. [Yoloswag package implementation](https://gitlab.com/jancsi.furo/helm-with-application/wikis/Yoloswag-package-implementation)
3. [Integration with Flask](https://gitlab.com/jancsi.furo/helm-with-application/wikis/Integration-with-Flask)
4. [Dockerizing the application](https://gitlab.com/jancsi.furo/helm-with-application/wikis/Dockerizing-the-application)
5. [Helm with application](https://gitlab.com/jancsi.furo/helm-with-application/wikis/Helm-with-application)

Used tools and their version:

- Python 3.6
- Flask Framework version 1.0.2
- Docker version 18.03.1-ce
- minikube version v0.33.1
- kubectl version v1.13.2
- helm version v2.12.3
