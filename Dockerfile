FROM python:3.6-alpine

LABEL maintainer="Janos Furo <jancsi.furo@gmail.com>"

COPY app/requirements.txt /app/requirements.txt
RUN pip install -r app/requirements.txt

COPY app /app
WORKDIR /app

ENTRYPOINT [ "python" ]

CMD [ "app.py" ]