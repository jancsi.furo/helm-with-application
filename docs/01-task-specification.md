# Hiring Task - Helm With Application

**Project Summary:**

* Create an application (in a language of your choice) which does the following
  * Write a program that prints the numbers from 1 to 50. But for multiples of three print “Yolo” instead of the number and for the multiples of five print “Swag”. For numbers which are multiples of both three and five print “YoloSwag”
* Create a docker image containing the application
* Create a helm chart that deploys the application
* Document everything you do, be able to present it at the interview for us. The crucial part of the solution is the code that you write.

**Project Goals:**

* Show familiarity with coding
* Show familiarity with docker
* Being able to pick up new skills if necessary.
* Show attention to detail and documentation skills.

**Bonus Points:**

* Use docker best practices 
* Test your code
* Make a small CLI script that starts minikube, build image and deploys the helm chart