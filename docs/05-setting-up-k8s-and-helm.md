# Application with Helm

Required steps:

- setting up minikube and kubectl
- creating a deployment and service for the yoloswag app using minikube
- creating a helm chart for the yoloswag app
- creating a small CLI script that starts minikube, build image and deploys the helm chart

## Setting up minikube

The installation process can be found in the [official docs](https://kubernetes.io/docs/tasks/tools/install-minikube/).

After minikube and kubectl successfully installed and we have the previously built yoloswag docker image, we can start minikube.

Open a terminal and start minikube:

```shell
minikube start
```

To be able to work with the docker daemon on the host, use the docker-env command in your shell:

```shell
eval $(minikube docker-env)
```

Build the docker image (from the project root folder):

```shell
docker build -t yoloswag:v1 .
```

Create a deployment using kubectl, then check if it was successful:

```shell
$ kubectl create deployment yoloswag --image=yoloswag:v1
deployment.apps/yoloswag created

$ kubectl get deployment yoloswag
NAME       READY   UP-TO-DATE   AVAILABLE   AGE
yoloswag   1/1     1            1           23s
```

Expose deployment as a NodePort service (using minikube this is the preferred way so we can access the service through minikube vm's opened port):

```shell
$ kubectl expose deployment yoloswag --type=NodePort --port=5000
service/yoloswag exposed

$ kubectl get service
NAME       TYPE       CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
yoloswag   NodePort   10.110.104.18   <none>        5000:32598/TCP   36s
```

Opening a new terminal and using the  `minikube dashboard`  command we can see our deployment and service in the kubernetes dashboard.

At this point we have a deployment which contains a running container (yoloswag:v1 image) and we have made a service that makes the yoloswag app accessible outside of the kubernetes cluster.

We know the port of the service from the previous command (in this example it is 32598), get the minikube vm's ip:

```shell
$ minikube ip
192.168.99.102
```

We can access the service at 192.168.99.102:32598 and appending the correct route to it we can get the translated yoloswag numbers:

```bash
$ curl http://192.168.99.102:32598/api/v1/yoloswag
[
  1, 
  2, 
  "Yolo", 
  4, 
  "Swag", 
  "Yolo", 
  7, 
  8, 
  "Yolo", 
  "Swag", 
  11, 
  "Yolo", 
  13, 
  14, 
  "YoloSwag", 
  16, 
  17, 
  "Yolo", 
  19, 
  "Swag", 
  "Yolo", 
  22, 
  23, 
  "Yolo", 
  "Swag", 
  26, 
  "Yolo", 
  28, 
  29, 
  "YoloSwag", 
  31, 
  32, 
  "Yolo", 
  34, 
  "Swag", 
  "Yolo", 
  37, 
  38, 
  "Yolo", 
  "Swag", 
  41, 
  "Yolo", 
  43, 
  44, 
  "YoloSwag", 
  46, 
  47, 
  "Yolo", 
  49, 
  "Swag"
]
```

Deleting the service and the deployment, then stopping minikube:

```bash
kubectl delete service yoloswag

kubectl delete deployment yoloswag

minikube stop
```

## Creating deployment and service description files

The more preferred way of handling deployments and services in kubernetes is to create description yaml files for them, making more managable and more clear what we want to do.

Basicly we want to create the same thing as above, but instead of manual configuration, we use a yaml file.

The `kubernetes-resources/deployment.yaml` file describe our deployment:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: yoloswag-deployment
spec:
  replicas: 1
  selector:
    matchLabels:
      app: yoloswag
  template:
    metadata:
      labels:
        app: yoloswag
    spec:
      containers:
      - name: yoloswag
        image: yoloswag:v1
        imagePullPolicy: Never
        ports:
        - containerPort: 5000
```

Usage:

```shell
$ kubectl create -f kubernetes-resources/deployment.yaml
deployment.apps/yoloswag-deployment created

$ kubectl get deployment yoloswag-deployment
NAME                  READY   UP-TO-DATE   AVAILABLE   AGE
yoloswag-deployment   1/1     1            1           14s
```

The `kubernetes-resources/service.yaml` file describe our service:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: yoloswag-service
spec:
  type: NodePort
  selector:
    app: yoloswag
  ports:
  - protocol: TCP
    port: 5000
    targetPort: http
```

Usage:

```shell
$ kubectl create -f kubernetes-resources/service.yaml
service/yoloswag-service created

$ kubectl get service yoloswag-service
NAME               TYPE       CLUSTER-IP       EXTERNAL-IP   PORT(S)          AGE
yoloswag-service   NodePort   10.106.118.132   <none>        5000:30331/TCP   12s

```

The testing of the service can be done like we did after setting up minikube.

## Creating helm chart

Install helm with snap:

```shell
sudo snap install helm --classic
```

I've chosen to generate a chart and modify it instead of creating the required files manually one by one. Although not all the generated features are used, it has the opportunity to be used later.

Generate a new chart (command from helm-chart folder):

```shell
helm create yoloswag
```

This command creates a chart with templates and default values, in the following structure:

![chart](img/yoloswag-chart.png)

The 2 important file for us is the templates/deployment.yaml and the templates/service.yaml. These 2 files have to do the same job as our previously created deplyoment.yaml and service.yaml files in the previous section.

In the templates/deployment.yaml file, we have this containers section by default:

```yaml
...
      containers:
        - name: {{ .Chart.Name }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          ports:
            - name: http
              containerPort: 80
              protocol: TCP
          livenessProbe:
            httpGet:
              path: /
              port: http
          readinessProbe:
            httpGet:
              path: /
              port: http
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
...
```

The yoloswag app does not have any livenessProbe and readinessProbe implemented (in order to keep this example simple), so we have to delete this section in order to be able to use the image. Our image uses port 5000, so we have to change it too:

```yaml
...
      containers:
        - name: {{ .Chart.Name }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          ports:
            - name: http
              containerPort: 5000
              protocol: TCP
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
...
```

The templates/service.yaml file does not require any modification.

The values.yaml hold the values for the template files. Its values (the relevant parts) by default are the following:

```yaml
...

image:
  repository: nginx
  tag: stable
  pullPolicy: IfNotPresent

...

service:
  type: ClusterIP
  port: 80

...
```

After modofying it to use our setup:

```yaml
...

image:
  repository: yoloswag
  tag: v1
  pullPolicy: Never

...

service:
  type: NodePort
  port: 5000

...
```

(In this example I don't use any ingress but I've left the files in the chart and left it disabled in the values.yaml.)

In the templates/NOTES.txt we can write some notes for the chart that will be printed after installation. In this note, there are a few lines of shell commands in order to help the user testing the application. Copying and pasting these lines in the terminal we can easily get the numbers that have been translated by the yoloswag app.

```shell
export NODE_PORT=$(kubectl get --namespace {{ .Release.Namespace }} -o jsonpath="{.spec.ports[0].nodePort}" services {{ include "yoloswag.fullname" . }})

export NODE_IP=$(minikube ip)

export SERVICE_PATH="/api/v1/yoloswag"
  
# to get the yoloswag numbers use this command:
curl http://$NODE_IP:$NODE_PORT$SERVICE_PATH
```

Finally we can set the basic infos for the chart in the Chart.yaml file.

## Installing the yoloswag helm chart

After setting up minikube and helm (with `helm init` command), we can install the chart:

```shell
$ helm install ./helm-chart/yoloswag --name yoloswag
NAME:   yoloswag
LAST DEPLOYED: Sat Feb  2 13:58:35 2019
NAMESPACE: default
STATUS: DEPLOYED

RESOURCES:
==> v1/Pod(related)
NAME                       READY  STATUS             RESTARTS  AGE
yoloswag-667b5bbf86-jllsc  0/1    ContainerCreating  0         0s

==> v1/Service
NAME      TYPE      CLUSTER-IP      EXTERNAL-IP  PORT(S)         AGE
yoloswag  NodePort  10.111.123.115  <none>       5000:30492/TCP  0s

==> v1/Deployment
NAME      DESIRED  CURRENT  UP-TO-DATE  AVAILABLE  AGE
yoloswag  1        1        1           0          0s


NOTES:
This chart is developed for minikube, it uses a NodePort service that can be accessed by running these commands:

  export NODE_PORT=$(kubectl get --namespace default -o jsonpath="{.spec.ports[0].nodePort}" services yoloswag)
  export NODE_IP=$(minikube ip)
  export SERVICE_PATH="/api/v1/yoloswag"
  
  # to get the yoloswag numbers use this command:
  curl http://$NODE_IP:$NODE_PORT$SERVICE_PATH
```

Using the commands in the NOTES section we can access the service.

Delete the installed chart and stop minikube with the following commands:

```shell
helm del --purge yoloswag

minikube stop
```

## Helper script

In the root folder, there is a script named `dev-setup.sh` that starts minikube, build image and deploys the helm chart.

The script is quite straight forward, it just reuses the commands that are used previously.

The only problem I found when testing the script is that I cannot use helm commands right after starting minikube though if I wait nearly half a minute, then everything is ok.

> The problem was the following:
>
>```bash
>$ helm install ./helm-chart/yoloswag --name yoloswag
>Error: Get https://10.96.0.1:443/version?timeout=32s: dial tcp 10.96.0.1:443: i/o timeout
>```
> I've found several issues on github with similar error messages but couldn't find one that is the same as in my case and the other suggested workarounds didn't changed anything.
>
> The workaround I've used in the script solved the problem temporarily but for a full understanding of why this happens when minikube starts needs more troubleshooting which may be out of the scope of this demo project.

Further analyzing the problem I've found that the kube-proxy daemonset is not ready right after starting minikube and probably this is causing the problem. For a workaround, I've added some waiting to the script and this way helm tries to install the chart after kube-proxy is in a ready state:

```bash
echo "Waiting for kube-proxy to be ready ..."
sleep 30

# wait for kube-proxy to be ready
for i in {1..30}
do
    READY=$(kubectl get daemonset.apps kube-proxy -n kube-system -o 'jsonpath={.status.numberReady}')
    echo "Waiting for kube-proxy to be ready ..."
    sleep 10

    if [ "$READY" -eq 1 ]
    then
        break  
    fi
done
```

With this workaround, the script working fine, but the amount of waiting can be different on different systems.

There is another script named `dev-teardown.sh` that deletes the yoloswag chart and stops minikube.