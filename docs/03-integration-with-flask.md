# Integration with Flask and creating the application

I've chosen to use [Flask](http://flask.pocoo.org/) for getting the YoloSwag application up and running as a service with a simple endpoint that can be used to retrieve the translated numbers.

In order to keep everything simple for this demo application, the webserver is not configured and it is set up by using it's the most basic form for development:  

- After instantiating the Flask app, it creates a yoloswag service and an api endpoint that handles HTTP GET requests, and as a response, it returns the numbers from 1 to 50 as requested in the specification.

You can run the server from the app folder by the following command:

```python
python app.py
```

This starts the server on the local machine, listening on port 5000, accessible by anyone on your network.

The flask app is in the `app/app.py` file.

## App requirements

The application's only requirement that is not part of the built-in python packages is Flask, so it needs to be defined in the `requirements.txt` file. This file is used by pip to install required python packages.

Usage:

```shell
pip install -r requirements.txt
```

## Testing

In order to make sure that our webserver is working correctly, I've used Flask's testing abilities and the unittest package.

There is only one test, that checks whether or not the response from the endpoint is ok and we have the required number of translated numbers.

You can run the test with the following command from the `app` folder:

```python
python -m unittest flask-test
```

The test is in the `app/flask-test.py` file.