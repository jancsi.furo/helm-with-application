# Dockerizing the application

Requirements for containerization:

- the container must have a python interpreter, preferred version is 3.6
- the app's requirements must be installed from `app/requirements.txt`
- set up a container to run the Flask server

Docker best practices considerations:

- keep the size of the Docker image low
  - choosing base image
  - analyze image size and app size
  - evaluate the necessity of multi-stage build
- keep the Dockerfile simple and clear
  - evaluate the necessity of multi-stage build

## Creating the Dockerfile

As a first approach, we could use a general ubuntu image, then install the python environment for ourselves, but it is way more than we need and there are also different official python images we can use.

The most common solution is to use an alpine based docker image. The size of the `python:3.6-alpine` image is 74.5MB (at the time of writing this) which is quite small, although it has everything we need for our application.

The most simple Dockerfile for the application is the following:

```docker
FROM python:3.6-alpine

LABEL maintainer="Janos Furo <jancsi.furo@gmail.com>"

COPY app /app
WORKDIR /app

RUN pip install -r requirements.txt

ENTRYPOINT [ "python" ]

CMD [ "app.py" ]
```

After building, the size of the image is 85MB.

## Layer caching

Leveraging Docker's layer caching we can optimize the speed of the build by copying the requirements.txt first and handle the installation, then copy the app's source code.

```docker
FROM python:3.6-alpine

LABEL maintainer="Janos Furo <jancsi.furo@gmail.com>"

COPY app/requirements.txt /app/requirements.txt
RUN pip install -r app/requirements.txt

COPY app /app
WORKDIR /app

ENTRYPOINT [ "python" ]

CMD [ "app.py" ]
```

When rebuilding the image, this way the installation of the requirements will be skipped if the reuirements.txt does not change.

The overall size of this demo project does not require such optimalisation like not to copy the whole app folder to the container, although we do not necessarily need the test files, it is good to keep in mind that it would not be the same with a bigger project.

## Multi-stage build

Using multistage builds means that dependencies can be built in one image and then imports into another. Rewriting the Dockerfile it looks like the following:

```docker
FROM python:3.6-alpine as base

LABEL maintainer="Janos Furo <jancsi.furo@gmail.com>"

FROM base as builder

RUN mkdir /install
WORKDIR /install

COPY app/requirements.txt /requirements.txt

# Install the requirements in the /install folder
RUN pip install --install-option="--prefix=/install" -r /requirements.txt

FROM base

COPY --from=builder /install /usr/local

COPY app /app
WORKDIR /app

ENTRYPOINT [ "python" ]

CMD [ "app.py" ]
```

This demo project does not necessarily need a multistage build because of its overall size, but it is a good option to lower the size of the image.

Using this, we can go down with the image size to 78.2MB.

## Build and test the image

Build the image with the following command from the base folder:

```shell
docker build -t yoloswag:v1 .
```

Optionally you can use the `--no-cache` flag to disable layer caching.

Running the docker container:

```shell
docker run -p 5000:5000 yoloswag:v1
```

Flask listens on port 5000, using the `-p 5000:5000` binds the port to the host, so we can open a browser and access the yoloswag app at `localhost:5000/api/vi/yoloswag`

![browser-result](img/browser-result-1.png)
