# Implementation of the yoloswag module

## Specification and requirements

The program's specification is the following:  
>Write a program that prints the numbers from 1 to 50. But for multiples of three print “Yolo” instead of the number and for the multiples of five print “Swag”. For numbers which are multiples of both three and five print “YoloSwag”

To solve this problem, the program has to be able to:  

  1. translate a number based on the given rule set
  2. do the translation for the numbers from 1 to 50 and print the result list

## Translation process

The process of the translation of a number can be viewed as a flow chart:  

![translation process](img/translation-process.png)

Using this workflow, every number can be translated, the results can be appended to a list, creating the required outcome.

## Implementation

I've chosen to use Python for implementation. The source code can be found in app/yoloswag/src/yoloswag.py  

The numbers (3, 5) and symbols ("Yolo", "Swag") which define the translation are handled as parameters, making it easy to handle possible future modifications and making the class and tests more robust. The number of required numbers handled the same way that can be modified.

The class uses the number and symbols given in the specification as default values.

## Tests

I've chosen to use Python's built-in unittest module for testing. The tests can be found in `app/yoloswag/tests/test_yoloswag.py` file.

You can run the tests from the `app/yoloswag` directory with the following command:  

```python
python -m unittest tests.test_yoloswag
```

The testcase covers the basic functionality of the number translation (the 4 possible outcomes) and the ability to create a list from the translated numbers.  