import unittest
import json
import app as yoloswag_application

class FlaskTest(unittest.TestCase):

    def setUp(self):
        yoloswag_application.app.testing = True
        self.client = yoloswag_application.app.test_client()


    def test_yoloswag_response(self):
        response = self.client.get('/api/v1/yoloswag')

        # Test whether the webserver responses with an OK
        self.assertEqual(response.status_code, 200)

        data = response.get_json()

        # The number of translated numbers must be 50 by default
        self.assertEqual(len(data), 50)

