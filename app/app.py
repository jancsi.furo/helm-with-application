
from flask import Flask, jsonify
from yoloswag.src.yoloswag import YoloSwag

# Instantiate the application
app = Flask(__name__)

# Using YoloSwag with default values
yoloswag_service = YoloSwag()

@app.route('/api/v1/yoloswag')
def yoloswag():
    """
    This endpoint handles the HTTP GET requests and returns the
    translated yoloswag number list, using YoloSwag with the defaults
    """
    translated_number_list = yoloswag_service.get_translated_numbers()

    if translated_number_list:
        return jsonify(translated_number_list)
    else:
        return 'Cannot translate yoloswag numbers!', 500


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)
    