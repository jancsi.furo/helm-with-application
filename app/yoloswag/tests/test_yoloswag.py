import unittest
import src.yoloswag as ys

class YoloSwagTests(unittest.TestCase):
    """
    TestCase class that handles the tests for the YoloSwag class

    Test the 4 possible outcomes of the translation:
        "Yolo" number, "Swag" number, "YoloSwag" number, original number

    Test the functionality to create a list of translated numbers with and without
    custom parameters
    """

    # Set up tests by instantiating a YoloSwag class.
    def setUp(self):
        self.yoloswag = ys.YoloSwag()
    

    def test_number_translates_to_yolo(self):
        yolo_number = self.yoloswag.get_yolo_divisor()

        result = self.yoloswag.translate_number(yolo_number)

        self.assertEqual(result, self.yoloswag.get_yolo_simbol())
    

    def test_number_translates_to_swag(self):
        swag_number = self.yoloswag.get_swag_divisor()

        result = self.yoloswag.translate_number(swag_number)

        self.assertEqual(result, self.yoloswag.get_swag_simbol())
    

    def test_number_translates_to_yoloswag(self):
        yoloswag_number = self.yoloswag.get_yolo_divisor() * self.yoloswag.get_swag_divisor()
        expected_result = self.yoloswag.get_yolo_simbol() + self.yoloswag.get_swag_simbol()

        result = self.yoloswag.translate_number(yoloswag_number)

        self.assertEqual(result, expected_result)
    

    def test_number_stays_number(self):
        simple_number = 1

        result = self.yoloswag.translate_number(simple_number)

        self.assertEqual(result, simple_number)


    def test_yoloswag_result_with_custom_setup(self):
        """
        The YoloSwag can be instantiated with different parameters, test whether it is 
        functioning as we expect it.

        The expected_result_list is created manually, for the given parameters above
        """
        local_yoloswag = ys.YoloSwag(div_yolo = 2, div_swag = 7)

        yolo = local_yoloswag.get_yolo_simbol()
        swag = local_yoloswag.get_swag_simbol()

        expected_result_list = [
            1, yolo, 3, yolo, 5, yolo, swag, yolo, 9, yolo, 11, yolo, 13, yolo + swag, 15
        ]

        result_list = local_yoloswag.get_translated_numbers(15)

        self.assertListEqual(result_list, expected_result_list)

    def test_yoloswag_result_with_defaults(self):
        """
        By default, the YoloSwag class uses the following parameters:
            __divisor_yolo = 3
            __divisor_swag = 5
        
        This function tests the translation with a manually created expected_result_list
        """
        
        # self.assertEqual(self.yoloswag.get_yolo_divisor(), 3)
        # self.assertEqual(self.yoloswag.get_swag_divisor(), 5)

        yolo = self.yoloswag.get_yolo_simbol()
        swag = self.yoloswag.get_swag_simbol()

        expected_result_list = [
            1, 2, yolo, 4, swag, yolo, 7, 8, yolo, swag, 11, yolo, 13, 14, yolo + swag
        ]

        result_list = self.yoloswag.get_translated_numbers()

        self.assertListEqual(result_list[0:15], expected_result_list)


if __name__ == '__main__':
    unittest.main()