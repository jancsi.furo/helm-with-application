#!/usr/bin/python

class YoloSwag:
    """
    The YolosWag class implements a simple number translation functionality based on
    a given rule set:

    For multiples of self.__divisor_yolo, the number is translated to self.__simbol_yolo, and for
    multiples of self.__divisor_swag, the number is translated to self.__simbol_swag, meanwhile for
    numbers which are multiples of both self.__divisor_yolo and self.__divisor_swag, the translation
    is the concatenation of self.__simbol_yolo and self.__simbol_swag. In other cases, the number 
    stays without translation. 
 
    """

    def __init__(self, div_yolo = 3, div_swag = 5):
        self.__divisor_yolo = div_yolo
        self.__divisor_swag = div_swag
        self.__default_end_number = 50
        self.__simbol_yolo = "Yolo"
        self.__simbol_swag = "Swag"


    def get_yolo_divisor(self):
        return self.__divisor_yolo
    
    def get_swag_divisor(self):
        return self.__divisor_swag

    def get_yolo_simbol(self):
        return self.__simbol_yolo

    def get_swag_simbol(self):
        return self.__simbol_swag


    def translate_number(self, number):
        """
        Translates the number if it's required according to the given rules.
        
        Parameters
        ----------
        number : int
            A single number that gets translated according to the given rules.
        
        Returns
        -------
        int or string
            The result of the translation is either one of the following:
                self.__simbol_yolo (string)
                self.__simbol_swag (string)
                self.__simbol_yolo + self.__simbol_swag (string)
                number (int)
        """
        modulo_swag = number % self.__divisor_swag
        modulo_yolo = number % self.__divisor_yolo

        translation = number

        if modulo_swag == 0 and modulo_yolo == 0:
            translation = self.__simbol_yolo + self.__simbol_swag
        elif modulo_swag == 0:
            translation = self.__simbol_swag
        elif modulo_yolo == 0:
            translation = self.__simbol_yolo

        return translation


    def get_translated_numbers(self, end_number = None):
        """
        Translate numbers from 1 to end_number creating a list from them

        Parameters
        ----------
        end_number : int
            Optional argument, the translation of numbers gos from 1 to end_number. If it is not
            defined, then, the self.__default_end_number value is used
        
        Returns
        -------
        list of int and string values

        """
        translated_list = []

        if not end_number:
            end_number = self.__default_end_number
        
        for i in range(1, end_number + 1):
            translated_list.append(self.translate_number(i))
        
        return translated_list

